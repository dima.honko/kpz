﻿using Robot.Common;
using System.Collections.Generic;

namespace DmytroKhonko.RobotChallange
{
    public abstract class MyRobot
    {
        public Robot.Common.Robot robot;
        protected IList<MyStation> myStations;
        public IList<Robot.Common.Robot> robots;
        abstract public RobotCommand DoMyStep();

        virtual public MyStation FindNearestFreeStation(Robot.Common.Robot movingRobot, IList<MyStation> stations)
        {
            MyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in stations)
            {
                if (station.isFree)
                {
                    int d = DistanceHelper.FindEnergyNeeded(station.Position, movingRobot.Position);
                    if (d < minDistance && d < movingRobot.Energy)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                    else if (d == minDistance)
                    {
                        if (station.energyStation.Energy > nearest.energyStation.Energy)
                        {
                            minDistance = d;
                            nearest = station;
                        }
                    }
                }
            }
            if (nearest == null)
            {
                return null;
            }
            else
            {
                return nearest;
            }
        }

        public Robot.Common.Robot FindNearestEnemy()
        {
            Robot.Common.Robot nearest = null;
            int maxProfit = 60;
            foreach (var robot in robots)
            {
                if (robot.OwnerName != "Dmytro Khonko")
                {
                    int d = DistanceHelper.FindEnergyNeeded(robot.Position, this.robot.Position);
                    int profit = (int)(robot.Energy * 0.1) - 20 - d;
                    if (profit > maxProfit && (d + 20) < this.robot.Energy)
                    {
                        maxProfit = profit;
                        nearest = robot;
                    }
                }
            }

            return nearest;
        }
    }
}