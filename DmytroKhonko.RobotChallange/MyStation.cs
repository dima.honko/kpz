﻿using System;
using System.Collections.Generic;
using Robot.Common;

namespace DmytroKhonko.RobotChallange
{
    public class MyStation
    {
        public EnergyStation energyStation;
        public int id;
        public Position Position;
        public bool isFree = true;

        public MyStation(EnergyStation energyStation, int id)
        {
            this.id = id;
            this.energyStation = energyStation;
            this.Position = energyStation.Position;
        }

        public Position findCloseWay(Robot.Common.Robot robot)
        {
            IList<Position> poslist = new List<Position>
            {
                new Position(Position.X - 1, Position.Y - 1),
                new Position(Position.X + 1, Position.Y + 1),
                new Position(Position.X - 1, Position.Y + 1),
                new Position(Position.X + 1, Position.Y - 1),
                new Position(Position.X - 1, Position.Y),
                new Position(Position.X + 1, Position.Y),
                new Position(Position.X, Position.Y - 1),
                new Position(Position.X, Position.Y + 1),
                new Position(Position.X, Position.Y),
                new Position(Position.X - 1, Position.Y - 1),
                new Position(Position.X - 1, Position.Y - 1)
            };
            Position minPosition = null;
            int mind = Int32.MaxValue;
            foreach (var posi in poslist)
            {
                int d = DistanceHelper.FindEnergyNeeded(posi, robot.Position);
                if (d < mind)
                {
                    mind = d;
                    minPosition = posi;
                }
            }
            return minPosition;
        }
    }
}