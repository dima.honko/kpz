﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace DmytroKhonko.RobotChallange
{
    public class DmtroKhonkoAlgorithm : IRobotAlgorithm
    {
        public string Author
        {
          get { return "Dmytro Khonko"; }
        }
        static IList<MyStation> myStations = new List<MyStation>();
       
        static Dictionary<Int32, MyRobot> myRobots= new Dictionary<Int32, MyRobot>();

        private int robotCount = 0;

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            int count = 0;
            if (myStations.Count == 0)
            {
                foreach (var station in map.Stations)
                {
                    MyStation myStation = new MyStation(station, count);
                    myStations.Add(myStation);
                    count++;
                }
            }
            else
            {
                foreach (var station in myStations)
                {
                    station.energyStation = map.Stations[count];
                }
            }

            if (myRobots.ContainsKey(robotToMoveIndex))
            {
                myRobots[robotToMoveIndex].robots = robots;
                myRobots[robotToMoveIndex].robot = movingRobot;
                return myRobots[robotToMoveIndex].DoMyStep();
            }

            MyRobot myRobotE = new RobotExplorer(movingRobot, myStations);
            myRobots.Add(robotToMoveIndex, myRobotE);
            myRobots[robotToMoveIndex].robots = robots;
            robotCount++;
            return myRobotE.DoMyStep();
        }
    }
}
