﻿using System;
using System.Collections.Generic;
using Robot.Common;

namespace DmytroKhonko.RobotChallange
{
    public class RobotExplorer : MyRobot
    {
        private static int count=0;
        private bool isAssigned = false;
        private MyStation myStation = null;
        private bool isParent = false;
        private Position mypos = null;
        private bool isFighter = false;
        public RobotExplorer(Robot.Common.Robot robot, IList<MyStation> myStations)
        {
            this.robot = robot;
            this.myStations = myStations;
            count++;
        }

        public override RobotCommand DoMyStep()
        {
            if (!isAssigned)
            {
                MyStation station = FindNearestFreeStation(robot, myStations);
                if (station != null)
                {
                    isAssigned = true;
                    myStation = station;
                    mypos = station.findCloseWay(robot);
                    station.isFree = false;
                    isFighter = false;
                }
            }
            if (isFighter)
            {
                myStation = FindNearestStation(robot, myStations);
            }
            if (myStation == null)
            {
                Robot.Common.Robot enemy = FindNearestEnemy();
                if (enemy == null)
                {
                    return new MoveCommand() { NewPosition =new Position(robot.Position.X, ++robot.Position.Y) };
                }
                isFighter = true;
                return new MoveCommand() { NewPosition = enemy.Position };
            }
            if (Math.Abs(robot.Position.X - myStation.Position.X)  +
                Math.Abs(robot.Position.Y - myStation.Position.Y) < 4) 
            {
                if (robot.Energy >= 100 && count < 100)
                {
                    MyStation nerMyStation = FindNearestFreeStation(robot, myStations);
                    
                    if (nerMyStation != null)
                    {
                        int d = DistanceHelper.FindEnergyNeeded(FindNearestFreeStation(robot, myStations).Position, robot.Position);
                        if (d < 50)
                        {
                            return new CreateNewRobotCommand() {NewRobotEnergy = 49};
                        }
                    }
                }

                if (myStation.energyStation.Energy < 20)
                {
                    isFighter = true;
                    myStation.isFree = true;
                    Robot.Common.Robot enemy = FindNearestEnemy();
                    if (enemy == null)
                    {
                        return new CollectEnergyCommand();
                    }
                    else
                    {
                        return new MoveCommand() { NewPosition = enemy.Position };
                    }
                }
                return new CollectEnergyCommand();
            }
            if (robot.Energy < DistanceHelper.FindEnergyNeeded(myStation.findCloseWay(robot), robot.Position))
            {
                Robot.Common.Robot enemy = FindNearestEnemy();
                if (enemy == null)
                {
                    return new MoveCommand() { NewPosition = new Position(robot.Position.X, ++robot.Position.Y) };
                }
                else
                {
                    isFighter = true;
                    myStation.isFree = false;
                    return new MoveCommand() { NewPosition = enemy.Position };
                }
            }
            return new MoveCommand() { NewPosition = myStation.findCloseWay(robot) };
        }

        public  MyStation FindNearestStation(Robot.Common.Robot movingRobot, IList<MyStation> stations)
        {
            MyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in stations)
            {

                int d = DistanceHelper.FindEnergyNeeded(station.Position, movingRobot.Position);
                if (d < minDistance && d <= movingRobot.Energy)
                {
                    minDistance = d;
                    nearest = station;
                }
                else if (d == minDistance)
                {
                    if (station.energyStation.Energy > nearest.energyStation.Energy)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            if (nearest == null)
            {
                return null;
            }
            else
            {
                return nearest;
            }
        }
    }
}