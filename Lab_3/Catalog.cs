﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_3
{
    public class Catalog
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Category> Categories { get; set; }
    }
}
