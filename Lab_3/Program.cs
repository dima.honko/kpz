using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var products = initializeProducts();
            var categories = initializeCategories();
            var catalogs = initializeCatalogs();

            Start1(products, categories);
            Console.WriteLine();
            Start2(categories, catalogs);
            Console.WriteLine();
            Start3(products);
            Console.WriteLine();
            Start4(products);
        }

        private static void Start4(List<Product> products)
        {
            var p = products.
                OrderByDescending(p => p.Name).
                GroupBy(p => p.Country).
                OrderBy(g => g.Key);

            foreach (var group in p)
            {
                Console.WriteLine("Country " + group.Key);
                foreach (var el in group)
                {
                    Console.WriteLine(el.Name + " " + el.Description);
                }
                Console.WriteLine();
            }

        }

        private static void Start3(List<Product> products)
        {
            Console.WriteLine("Products sorted by name");
            var sortedProductsArray = products.ToArray();
            Array.Sort(sortedProductsArray, new ProdNameComparer());
            foreach (var el in sortedProductsArray)
            {
                Console.WriteLine(el.Name + " " + el.Description);
            }
        }

        private static void Start2(List<Category> categories, List<Catalog> catalogs)
        {
            var dict = new Dictionary<String, List<String>>();

            foreach (var catalog in catalogs)
            {
                dict.Add(catalog.Name, new List<string>());
            }

            foreach (var key in dict.Keys)
            {
                dict.GetValueOrDefault(key).
                    AddRange(categories.
                        Where(ts => ts.CatalogId == catalogs.
                                                First(tr => tr.Name == key).Id).
                        Select(ts => ts.Name).
                        OrderBy(n => n).
                        ToList());
            }

            dict.Write();
        }

        private static void Start1(List<Product> products, List<Category> categories)
        {
            var category = categories.First(t => t.Name == "Bread");
            var select = products.Where(p => p.CategoryId == category.Id).Select(p => new
            {
                prodName = p.Name,
                prodDesc = p.Description
            }).ToList();

            Console.WriteLine(category.Name + " Products:");
            select.ForEach(s => Console.WriteLine(s.prodName + " " + s.prodDesc));
        }

        private static List<Catalog> initializeCatalogs()
        {
            return new List<Catalog>()
            {
                new Catalog() { Id = 1, Name = "Bakery"},
                new Catalog() { Id = 2, Name = "Milk Based"},
                new Catalog() { Id = 3, Name = "Alcohol"},
                new Catalog() { Id = 4, Name = "Meat"}
            };
        }

        private static List<Category> initializeCategories()
        {
            return new List<Category>()
            {
                new Category() { Id = 1, Name = "Bread", CatalogId = 1},
                new Category() { Id = 2, Name = "Milk", CatalogId = 2},
                new Category() { Id = 3, Name = "Butter", CatalogId = 2},
                new Category() { Id = 4, Name = "Beer", CatalogId = 3},
                new Category() { Id = 5, Name = "Vodka", CatalogId = 3},
                new Category() { Id = 6, Name = "Raw Meat", CatalogId = 4},
                new Category() { Id = 7, Name = "Cooked meat", CatalogId = 4}
            };
        }

        private static List<Product> initializeProducts()
        {
            return new List<Product>()
            {
                new Product() { Id = 1, Name = "Bread",Description = "White Bread", Country = "Ukraine", CategoryId = 1 },
                new Product() { Id = 2, Name = "Bread",Description = "Black Bread", Country = "Ukraine", CategoryId = 1 },
                new Product() { Id = 3, Name = "Milk",Description = "No fat", Country = "Poland" , CategoryId = 2},
                new Product() { Id = 4, Name = "Milk",Description = "3.4% fat", Country = "Ukraine", CategoryId = 2 },
                new Product() { Id = 5, Name = "Butter",Description = "34% fat", Country = "Ukraine", CategoryId = 3},
                new Product() { Id = 6, Name = "Beer", Description = "Light Beer", Country = "Germany", CategoryId = 4 },
                new Product() { Id = 7, Name = "Vodka", Description = "Stolichnaya ", Country = "Russia", CategoryId = 5 },
                new Product() { Id = 8, Name = "Hamon", Description = "finest ham", Country = "Spain", CategoryId = 6 },
                new Product() { Id = 9, Name = "Sausage",Description = "chicken sausage", Country = "Slovakia", CategoryId = 7 }
            };
        }
    }

}
