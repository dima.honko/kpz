using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Lab_3
{
    public class ProdNameComparer : IComparer<Product>
    {
        public int Compare([AllowNull] Product x, [AllowNull] Product y)
        {
           
            if (x?.Name == null) return -1;
            if (y?.Name == null) return 1;
            if (x.Name == null && y.Name == null) return 0;
            return string.Compare(x.Name, y.Name, StringComparison.Ordinal);
        }
    }
}
