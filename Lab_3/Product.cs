﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_3
{
    public class Product
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
        public Category Category { get; set; }

        public int CategoryId { get; set; }
    }
}
