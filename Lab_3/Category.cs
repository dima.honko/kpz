﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab_3
{
    public class Category
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public Catalog Catalog { get; set; }

        public int CatalogId { get; set; }
        public List<Product> Players { get; set; }
    }
}
