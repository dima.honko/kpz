﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
//using Robot = Robot.Common.Robot;

namespace DmytroKhonko.RobotChallange.Test
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestCollectEnergy()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation(){Position = new Position(1,1), Energy = 50});
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot(){ Position = new Position(2,1), Energy = 50});
            RobotCommand c = new DmtroKhonkoAlgorithm().DoStep(robots, 0 ,map);

            Assert.IsTrue(c is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestCreateRobot()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 300 });
            RobotCommand c = new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);

            Assert.IsTrue(c is CreateNewRobotCommand);

        }

        [TestMethod]
        public void TestMethodMove()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 3), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 300 });
            RobotCommand c = new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);
            Assert.IsTrue(c is MoveCommand);
        }

        [TestMethod]
        public void TestMethodChooseStation()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 1), Energy = 100 });
            map.Stations.Add(new EnergyStation() { Position = new Position(0, 1), Energy = 200 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(10, 1), Energy = 200 });
            MoveCommand mc = (MoveCommand) new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);
            
            Assert.AreEqual(mc.NewPosition,map.Stations[1].Position);
        }

        [TestMethod]
        public void TestLeaveEmptyStation()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 1), Energy = 0 });
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 1), Energy = 200 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 1), Energy = 200 });
            MoveCommand mc = (MoveCommand)new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);

            Assert.AreEqual(mc.NewPosition, map.Stations[1].Position);
        }

        [TestMethod]
        public void TestRobotFight()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 1), Energy = 200 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 1), Energy = 200 , OwnerName = "Dmytro Khonko" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 1), Energy = 200, OwnerName = "Test" });
            MoveCommand mc = (MoveCommand)new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);

            Assert.AreEqual(mc.NewPosition, map.Stations[0].Position);
        }

        [TestMethod]
        public void TestNoStationsFound()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 1), Energy = 0 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 1), Energy = 200, OwnerName = "Dmytro Khonko" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 1), Energy = 200, OwnerName = "Dmytro Khonko" });
            MoveCommand mc = (MoveCommand)new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);
            robots[0].Position.X++;
            Assert.AreEqual(mc.NewPosition, robots[0].Position);
        }

        [TestMethod]
        public void TestStationTaken()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 1), Energy = 200, OwnerName = "Dmytro Khonko" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 1), Energy = 100, OwnerName = "Test" });
            MoveCommand mc = (MoveCommand)new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);
            robots[0].Position.X--;
            Assert.AreEqual(mc.NewPosition, robots[0].Position);
        }

        [TestMethod]
        public void TestMoveFromStation()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 1), Energy = 200, OwnerName = "Dmytro Khonko" });
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 1), Energy = 100,OwnerName = "Dmytro Khonko" });
            MoveCommand mc = (MoveCommand)new DmtroKhonkoAlgorithm().DoStep(robots, 1, map);
            robots[1].Position.X--;
            Assert.AreEqual(mc.NewPosition, robots[1].Position);
        }

        [TestMethod]
        public void TestFrendlyFire()
        {
            Map map = new Map();
            map.Stations.Add(new EnergyStation() { Position = new Position(20, 1), Energy = 200 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(15, 1), Energy = 200, OwnerName = "Dmytro Khonko"});
            robots.Add(new Robot.Common.Robot() { Position = new Position(20, 1), Energy = 200, OwnerName = "Dmytro Khonko"});
            MoveCommand mc = (MoveCommand)new DmtroKhonkoAlgorithm().DoStep(robots, 0, map);
            robots[0].Position.X--;
            Assert.AreEqual(mc.NewPosition, robots[0].Position);
        }
    }
}

