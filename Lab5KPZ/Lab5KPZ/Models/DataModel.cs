﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Lab5KPZ.File;

namespace Lab5KPZ.Models
{
    [DataContract]
    class DataModel
    {
        [DataMember]
        public IEnumerable<Seller> Sellers { get; set; }
        [DataMember]
        public IEnumerable<Product> Products { get; set; }

        public DataModel()
        {
            this.Sellers = new List<Seller>() { 
                new Seller() { Name = "Ashan", Image = "C:\\Users\\svyat\\source\\repos\\Lab5KPZ\\Lab5KPZ\\images\\ashan.jpg", Place = "Lviv"}, 
                new Seller() { Name = "Silpo", Image = "C:\\Users\\svyat\\source\\repos\\Lab5KPZ\\Lab5KPZ\\images\\silpo.jpg", Place = "Kiyv" } };
            this.Products = new List<Product>() {
                new Product() { Name = "Кпупа", Description = "Крупа", Price = 1, Image = "C:\\Users\\svyat\\source\\repos\\Lab5KPZ\\Lab5KPZ\\images\\images.jpg" },
                new Product() { Name = "Рис", Description = "Рис", Price = 2, Image = "C:\\Users\\svyat\\source\\repos\\Lab5KPZ\\Lab5KPZ\\images\\rise.jpg" },
                new Product() { Name = "Пшоно", Description = "Пшоно", Price = 0.5, Image = "C:\\Users\\svyat\\source\\repos\\Lab5KPZ\\Lab5KPZ\\images\\pshono.jpg" }};
        }

        public static string path = "mydata.dat";
        public static DataModel Load()
        {
            if (System.IO.File.Exists(path))
            {
                return Serializer.DeserializeItem(path);
            }
            return new DataModel();
        }

        public void Save()
        {
            Serializer.SerializeData(path,this);
        }
    }
}
