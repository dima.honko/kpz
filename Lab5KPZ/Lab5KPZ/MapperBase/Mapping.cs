﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Lab5KPZ.Models;
using Lab5KPZ.ViewModels;

namespace Lab5KPZ.MapperBase
{
    public class Mapping
    {

        public void Create()
        {
           Mapper.CreateMap<DataModel, DataViewModel>();
           Mapper.CreateMap<DataViewModel, DataModel>();
           Mapper.CreateMap<Seller, SellerModelView>();
           Mapper.CreateMap<SellerModelView, Seller>();
           Mapper.CreateMap<Product, ProductModelView>();
           Mapper.CreateMap<ProductModelView, Product>();
        }
    }
}
