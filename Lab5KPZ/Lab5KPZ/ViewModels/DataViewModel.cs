﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Lab5KPZ.Models;

namespace Lab5KPZ.ViewModels
{
    class DataViewModel : ViewModelBase
    {
        public DataViewModel()
        {
            SetControllVisibility = new Command(ControllVisibility);
            this.DeleteProductCommand = new Command(async (object parameter) =>
            {
                this.Products.Remove(parameter as ProductModelView);
            });
        }

        private string _visibleControl = "Products";

        public string VisibleControl
        {
            get
            {
                return _visibleControl;
            }
            set
            {
                _visibleControl = value;
                OnPropertyChanged("VisibleControl");
            }
        }

        public ICommand DeleteProductCommand { get; set; }
        public ICommand SetControllVisibility { get; set; }
        public void ControllVisibility(object args)
        {
            VisibleControl = args.ToString();
        }


        public ObservableCollection<SellerModelView> _Sellers;
       

        public ObservableCollection<SellerModelView> Sellers
        {
            get
            {
                return _Sellers;
            }
            set
            {
                _Sellers = value;
                OnPropertyChanged("Sellers");
            }
        }

        private ObservableCollection<ProductModelView> _Products;
        public ObservableCollection<ProductModelView> Products
        {
            get
            {
                return _Products;
            }
            set
            {
                _Products = value;
                OnPropertyChanged("Products");
            }
        }
    }
}
