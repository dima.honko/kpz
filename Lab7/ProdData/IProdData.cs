﻿
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab7.Models;

namespace Lab7.ProdData
{
    public interface IProdData
    {
        List<Product> GetProducts();
        Product GetProduct(int id);
        void AddProduct(Product product);
        void DeleteProduct(Product product);
        Product UpdateProduct(Product product);
        List<Category> GetCategories();
        Category GetCategory(int id);
        void AddCategory(Category category);
        void DeleteCategory(Category category);
        Category UpdateCategory(Category category);
    }
}
