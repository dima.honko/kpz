﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab7.Models;

namespace Lab7.ProdData
{
    public class SqlDataB : IProdData
    {
        private MyDbContext _myDb;
        public SqlDataB(MyDbContext myDb)
        {
            _myDb = myDb;
        }

        public List<Product> GetProducts()
        {
            return _myDb.Products.ToList();
        }

        public Product GetProduct(int id)
        {
            return _myDb.Products.ToList().SingleOrDefault(x => x.Id == id);
        }

        public void AddProduct(Product product)
        {
            _myDb.Products.Add(product);
        }

        public void DeleteProduct(Product product)
        {
            _myDb.Products.Remove(product);
        }

        public Product UpdateProduct(Product product)
        {
            var odlProd = GetProduct(product.Id);
            odlProd.Name = product.Name;
            odlProd.Description = product.Description;
            return odlProd;
        }

        public List<Category> GetCategories()
        {
            return _myDb.Categories.ToList();
        }

        public Category GetCategory(int id)
        {
            return _myDb.Categories.ToList().SingleOrDefault(x => x.Id == id);
        }

        public void AddCategory(Category category)
        {
            _myDb.Categories.Add(category);
        }

        public void DeleteCategory(Category category)
        {
            _myDb.Categories.Remove(category);
        }

        public Category UpdateCategory(Category category)
        {
            var odlCategory = GetCategory(category.Id);
            odlCategory.Name = category.Name;
            return odlCategory;
        }
    }
}
