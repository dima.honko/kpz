﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Lab7.Models;
using Lab7.ProdData;
using Microsoft.AspNetCore.Http;

namespace Lab7.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public partial class CategoryController : ControllerBase
    {
        private IProdData _prodData;

        public CategoryController(IProdData prodData)
        {
            _prodData = prodData;
        }

        [HttpGet]
        public IActionResult GetCategories()
        {
            return Ok(_prodData.GetCategories());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetCategory(int id)
        {
            return Ok(_prodData.GetCategory(id));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public void AddCategory(Category category)
        {
            _prodData.AddCategory(category);
        }

        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteCategory(int id)
        {
            var category = _prodData.GetCategory(id);

            if (category != null)
            {
                _prodData.DeleteCategory(category);
                return Ok();
            }

            return NotFound();
        }

        [HttpPatch]
        [Route("{id}")]
        public IActionResult UpdateProduct(int id, Category newprod)
        {
            var category = _prodData.GetCategory(id);

            if (category != null)
            {
                newprod.Id = id;
                return Ok(_prodData.UpdateCategory(newprod));
            }
            return NotFound();
        }
    }
}
