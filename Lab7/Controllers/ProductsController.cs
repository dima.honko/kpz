﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab7.Models;
using Lab7.ProdData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Lab7.Controllers
{
   
    [ApiController]
    [Route("api/[controller]")]
    public partial class ProductsController : ControllerBase
    {
        private IProdData _prodData;
        public ProductsController(IProdData prodData)
        {
            _prodData = prodData;
        }
        [HttpGet]
        public IActionResult GetProducts()
        {
            return Ok(_prodData.GetProducts());
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetProduct(int id)
        {
            return Ok(_prodData.GetProduct(id));
        }

        [HttpPost]
        public void AddProduct(Product product)
        {
            _prodData.AddProduct(product);
        }
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var product = _prodData.GetProduct(id);

            if (product != null)
            {
                _prodData.DeleteProduct(product);
                return Ok();
            }
            return NotFound();
        }

        [HttpPatch]
        [Route("{id}")]
        public IActionResult UpdateProduct(int id, Product newprod)
        {
            var product = _prodData.GetProduct(id);

            if (product != null)
            {
                newprod.Id = id;
                return Ok(_prodData.UpdateProduct(newprod));
            }
            return NotFound();
        }
    }
}
