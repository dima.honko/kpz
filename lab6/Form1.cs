﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab6
{
    public partial class Form1 : Form
    {
        MyDbContext myDb = new MyDbContext();
        private string connectionString = ConfigurationManager.AppSettings["connectionString"];

        public Form1()
        {
            InitializeComponent();

            InitFields();

        }

        private void InitFields()
        {
            myDb = new MyDbContext();
            myDb.Products.Load();
            dataGridView1.DataSource = myDb.Products.Local.ToBindingList();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxName.Text) || String.IsNullOrEmpty(textBoxName.Text))
                return;

            var newProduct = new Product()
            {
                Name = textBoxName.Text,
                Description = textBoxDescription.Text
            };

            var watch = new Stopwatch();
            watch.Start();

            myDb.Products.Add(newProduct);
            myDb.SaveChanges();

            watch.Stop();
            timelabel.Text = watch.ElapsedTicks.ToString();

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells[0] == null)
            {
                return;
            }

            var watch = new Stopwatch();
            watch.Start();
            Product product = (Product)dataGridView1.CurrentRow.DataBoundItem;
            myDb.Products.Remove(product);
            myDb.SaveChanges();

            watch.Stop();
            timelabel.Text = watch.ElapsedTicks.ToString();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedCells[0] == null)
            {
                return;
            }

            var watch = new Stopwatch();
            watch.Start();

            Product product = (Product)dataGridView1.CurrentRow.DataBoundItem;

            product.Name = textBoxName.Text;
            product.Description = textBoxDescription.Text;

            myDb.SaveChanges();

            watch.Stop();
            timelabel.Text = watch.ElapsedTicks.ToString();
            InitFields();
        }


        private void buttonCreateAdo_Click(object sender, EventArgs e)
        {
            var query = $" INSERT INTO Products (Name, Description) VALUES ('{textBoxName.Text}', '{textBoxDescription.Text}')";

            var watch = new Stopwatch();
            watch.Start();


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
            watch.Stop();

            InitFields();
            timelabel.Text = watch.ElapsedTicks.ToString();
        }

        private void buttonDeleteAdo_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedCells[0] == null)
            {
                return;
            }

            Product product = (Product)dataGridView1.CurrentRow.DataBoundItem;

            var query = $"DELETE FROM Products WHERE Id = {product.Id}";
            var watch = new Stopwatch();
            watch.Start();


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
            watch.Stop();
            timelabel.Text = watch.ElapsedTicks.ToString();
            myDb.SaveChanges();
            InitFields();
        }

        private void buttonUpdateAdo_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedCells[0] == null)
            {
                return;
            }
            Product product = (Product)dataGridView1.CurrentRow.DataBoundItem;

            var query = $" UPDATE Products SET Name = '{textBoxName.Text}', Description =  '{textBoxDescription.Text}'" +
                $"  WHERE Id = {product.Id}";

            var watch = new Stopwatch();
            watch.Start();


            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
            }
            watch.Stop();
            myDb.SaveChanges();
            InitFields();
            timelabel.Text = watch.ElapsedTicks.ToString();
        }
    }
}
