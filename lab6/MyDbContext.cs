﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    class MyDbContext : DbContext
    {
        public MyDbContext()
            : base("KhonkoLab6")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Catalog> Catalogs { get; set; }
        public virtual DbSet<Product> Products { get; set; }
    }
}
